FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
RUN mkdir /app
RUN echo "hello world" >> /app/test.sh
COPY build/libs/designPatterns-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/designPatterns-0.0.1-SNAPSHOT.jar"]

