package com.example.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
@Data
@RequiredArgsConstructor
public class PaymentServiceImpl2 implements IPayment{

    private Long taxAmount;
    @Override
    public void pay(String account, Double amount) {
        System.out.println("Account"+account+" amount" + amount);

    }
}
