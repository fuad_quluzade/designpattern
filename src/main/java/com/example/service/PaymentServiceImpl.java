package com.example.service;

import org.springframework.stereotype.Component;

//@Component
public class PaymentServiceImpl implements IPayment{

//    Singleton pattern
private static PaymentServiceImpl instance;
    private PaymentServiceImpl(){}
    @Override
    public void pay(String account, Double amount) {
        System.out.println("Account"+account+" amount" + amount);
    }

    public static PaymentServiceImpl getInstance(){
       if (instance==null){
           synchronized (PaymentServiceImpl.class){
               if (instance==null)
           instance=new PaymentServiceImpl();
           }
       }
       return instance;
    }
}
