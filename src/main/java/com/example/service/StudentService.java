package com.example.service;

import com.example.model.Student;
import com.example.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService implements Printable{

    @Autowired
     private StudentRepository studentRepository;

    public List<Student> getStudents(){
        return studentRepository.findAll();
    }

    public
     Student getStudent(Long id){
     return studentRepository.findById(id).orElseThrow(()->new RuntimeException("id not fount"));
    }

    public Student createStudent(Student student){
         return studentRepository.save(student);
    }


    public Student update(Long id, Student student){
          getStudent(id);
          student.setId(id);
        return   studentRepository.save(student);
    }

    public void delete( Long id ){
         studentRepository.deleteById(id);
    }

    @Override
    public void print(String msg) {

    }
}
