package com.example.service;

import org.springframework.stereotype.Component;


public interface IPayment {

     void pay(String account,Double amount);
}
