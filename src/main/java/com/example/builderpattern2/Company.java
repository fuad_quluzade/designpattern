package com.example.builderpattern2;

public abstract class Company extends CD {
    public abstract int price();
}
