package com.example.builderpattern2;

import org.springframework.cache.annotation.Cacheable;

public class CDBuilder {
    public CDType buildSony(){
        CDType cdType=new CDType();
        cdType.addItem(new Sony());
        return cdType;
    }

    public CDType buildSamsung(){
        CDType cdType= new CDType();
        cdType.addItem(new Samsung());
        return cdType;
    }
}
