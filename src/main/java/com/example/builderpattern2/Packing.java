package com.example.builderpattern2;

public interface Packing {
    String pack();
    int price();
}
