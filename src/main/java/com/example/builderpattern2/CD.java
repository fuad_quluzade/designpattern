package com.example.builderpattern2;

public abstract class CD implements Packing {
    public abstract String pack();
}
