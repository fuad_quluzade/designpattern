package com.example.prototypeexample;

public interface Prototype {

     Prototype getClone();

}
