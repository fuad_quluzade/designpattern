package com.example.singletonexample;

public class A {
    //Early, instance will be created at load time
    private static A obj=new A();
    private A(){}

    public static A getObj(){
        return obj;
    }

    public void doSomething(){
        System.out.println("A");
    }
}
