package com.example.singletonexample;

public class C {
    public static C obj;

    private C() {
    }

    public static C getObj() {
        if (obj == null) {
            synchronized (C.class) {
                if (obj == null) {
                    obj = new C();
                }
            }
        }
        return obj;
    }
}
