package com.example.builderpattern;

public interface Packing {
     String pack();
}
