package com.example.builderpattern;

public interface Item {
     String name();
     Packing packing();
     float price();
}
