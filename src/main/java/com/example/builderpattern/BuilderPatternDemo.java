package com.example.builderpattern;

public class BuilderPatternDemo {
    public static void main(String[] args) {
        MealBuilder mealBuilder=new MealBuilder();
        Meal vegMeal=mealBuilder.VegMeal();
        System.out.println("Veg Meal");
        vegMeal.showItems();
        System.out.println("total cost =" + vegMeal.getCost());

        Meal nonVegMeal=mealBuilder.NonVegan();
        System.out.println("non veg");
        nonVegMeal.showItems();
        System.out.println("total cost = " + nonVegMeal.getCost());
    }
}
