package com.example;

import com.example.configuration.AppDetails;
import com.example.factory.IPrinter;
import com.example.factory.PrinterFactory;
import com.example.factory.test;
import com.example.factoryimpl.GetPlanFactory;
import com.example.factoryimpl.Plan;
import com.example.model.Student;
import com.example.service.IPayment;
import com.example.service.PaymentServiceImpl;
import com.example.service.Printable;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
@RequiredArgsConstructor
public class DesignPatternsApplication implements CommandLineRunner {
    final IPayment iPayment = PaymentServiceImpl.getInstance(); //singleton
    final IPayment iPayment2 = PaymentServiceImpl.getInstance();

    final IPayment iPayment3; //dependency injection
     final IPayment iPayment4;

    @Autowired
    @Qualifier("LG")
    final IPrinter iPrinter;
    @Autowired
    @Qualifier("Do")
    final Plan plan;

    final AppDetails appDetails;

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternsApplication.class, args);
        test a=new test("fuad");
        System.out.println(a);
//       A a= A.getObj();
//       a.doSomething();
//       B b=B.getB();
//       b.doSomething();
//        A a=new A(); not work because of private construktor
//        Student student1=new Student();
//        System.out.println(student1);
//        Printable printable = new Printable() {
//            @Override
//            public void print(String msg) {
//                System.out.println(msg);
//            }
//        };
//        printable.print(" Print message to console....1");
//
//        // with lambda expression
//        Printable withLambda = (msg) -> System.out.println(msg);
//        withLambda.print(" Print message to console....2");
//    List<String> list= Arrays.asList("F","U","A");
//    list.forEach(System.out::println);
//    Stream<String> stream=list.stream();
//    stream.forEach(System.out::print);
//        String[] geekslist = { "1", "2", "4", "2", "1", "2",
//                "3", "1", "3", "4", "3", "3" };
//
//        // Creating a list and removing
//        // elements without use of singleton()
//        List geekslist1 = new ArrayList(Arrays.asList(geekslist));
//        System.out.println("Original geeklist1: " + geekslist1);
////
////        geekslist1.removeAll("1");
//        System.out.println("geekslist1 after removal of 1 without"
//                + " singleton " + geekslist1);
//        geekslist1.remove("1");
//        System.out.println("geekslist1 after removal of 1 without"
//                + " singleton " + geekslist1);
//        geekslist1.remove("2");
//        System.out.println("geekslist1 after removal of 2 without"
//                + " singleton " + geekslist1);

    }


    @Override
    public void run(String... args) throws Exception {
//        System.out.println("Runner"+ iPayment);
//        System.out.println("Runner"+ iPayment2);
//        System.out.println("Runner" + iPayment3);
//        System.out.println("Runner" + iPayment4);
//        IPrinter hp = PrinterFactory.getInstance("HP");
//        IPrinter lg = PrinterFactory.getInstance("LG");
//        Plan p= GetPlanFactory.getPlan("DOMESTICPLAN");
//        System.out.println(plan);
//        System.out.println(hp);
//        System.out.println(lg);
//        System.out.println(iPrinter);

//        Student student=Student.builder() //clasin icinde coxlu optional fieldler varsa, hansiki requared deil
//                .age(9)                     // onda bunun instansini rahatliqla yaratmaq ucun builder patter isledirik
//                .firstName("Fuad")
//                .lastName("Quluzade")
//                .build();

//        System.out.println(student);

//
//        System.out.println(appDetails);
//        appDetails.getGrades()
//                .forEach((k,v)-> System.out.println("Key"+k+"value"+v));



    }
}
