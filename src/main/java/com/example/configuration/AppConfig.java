package com.example.configuration;

import com.example.factory.HP;
import com.example.factory.IPrinter;
import com.example.factory.LG;
import com.example.factoryimpl.DomesticPlan;
import com.example.factoryimpl.Plan;
import com.example.service.IPayment;
import com.example.service.PaymentServiceImpl;
import com.example.service.PaymentServiceImpl2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.atomic.AtomicInteger;

@Configuration
public class AppConfig {

AtomicInteger atomicInteger=new AtomicInteger();
    @Bean
    @Scope("prototype")
    public IPayment paymentBean(){
        System.out.println("created new payment service " + atomicInteger.incrementAndGet());
         PaymentServiceImpl2 iPayment= new PaymentServiceImpl2();
         iPayment.setTaxAmount(18L);
         return iPayment;

    }

    @Bean("HP")
    public IPrinter HP(){
        return new HP();
    }

    @Bean("LG")
    @Primary
    public IPrinter LG(){
        return new LG();
    }

    @Bean("Do")
    public Plan Domestic(){return new DomesticPlan();
    }
}
