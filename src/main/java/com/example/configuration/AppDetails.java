package com.example.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Data
@Component
@ConfigurationProperties("az.lms.app")
public class AppDetails {

    private String appName;

    private String version;

    private String header;

    private List<String> contributors;

    private Map<String,Long> grades;
}

